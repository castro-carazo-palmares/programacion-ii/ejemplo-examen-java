/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import data.DatabaseManager;
import entidades.Genero;
import entidades.Pelicula;
import java.util.List;

/**
 *
 * @author Esteban Ramírez Martínez
 */
public class Servicio {

    DatabaseManager controlador = new DatabaseManager();

    public List<Genero> getGeneros() {
        controlador.abrirBD();
        List<Genero> generos = controlador.getGeneros();
        controlador.cerrarBD();
        return generos;
    }

    public List<Pelicula> getPeliculas() {
        controlador.abrirBD();
        List<Pelicula> peliculas = controlador.getPeliculas();
        controlador.cerrarBD();
        return peliculas;
    }

    public Genero getGenero(int id) {
        controlador.abrirBD();
        Genero genero = controlador.getGenero(id);
        controlador.cerrarBD();
        return genero;
    }

    public Pelicula getPelicula(int id) {
        controlador.abrirBD();
        Pelicula pelicula = controlador.getPelicula(id);
        controlador.cerrarBD();
        return pelicula;
    }

    public String postGenero(Genero genero) {
        controlador.abrirBD();
        int id = controlador.insertarGenero(genero);
        controlador.cerrarBD();
        return "El género con el id " + id + " ha sido creado satisfactoriamente.";
    }

    public String postPelicula(Pelicula pelicula) {
        controlador.abrirBD();
        int id = controlador.insertarPelicula(pelicula);
        controlador.cerrarBD();
        return "La película con el id " + id + " ha sido creada satisfactoriamente.";
    }

    public String putGenero(Genero genero) {
        controlador.abrirBD();
        Genero consulta = controlador.getGenero(genero.getId());
        controlador.cerrarBD();

        if (consulta != null) {
            controlador.abrirBD();
            controlador.actualizarGenero(genero);
            controlador.cerrarBD();
            return "El género con el id " + genero.getId() + " ha sido actualizado satisfactoriamente.";
        } else {
            return "El género no se encuentra en la base de datos.";
        }
    }

    public String putPelicula(Pelicula pelicula) {
        controlador.abrirBD();
        Pelicula consulta = controlador.getPelicula(pelicula.getId());
        controlador.cerrarBD();

        if (consulta != null) {
            controlador.abrirBD();
            controlador.actualizarPelicula(pelicula);
            controlador.cerrarBD();
            return "La película con el id " + pelicula.getId() + " ha sido actualizada satisfactoriamente.";
        } else {
            return "La película no se encuentra en la base de datos.";
        }
    }
    
    public String deleteGenero(int id) {
        controlador.abrirBD();
        Genero consulta = controlador.getGenero(id);
        controlador.cerrarBD();

        if (consulta != null) {
            controlador.abrirBD();
            controlador.eliminarGenero(id);
            controlador.cerrarBD();
            return "El género con el id " + id + " ha sido eliminado satisfactoriamente.";
        } else {
            return "El género no se encuentra en la base de datos.";
        }
    }
    
    public String deletePelicula(int id) {
        controlador.abrirBD();
        Pelicula consulta = controlador.getPelicula(id);
        controlador.cerrarBD();

        if (consulta != null) {
            controlador.abrirBD();
            controlador.eliminarPelicula(id);
            controlador.cerrarBD();
            return "La película con el id " + id + " ha sido eliminada satisfactoriamente.";
        } else {
            return "La película no se encuentra en la base de datos.";
        }
    }

}
