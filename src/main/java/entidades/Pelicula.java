/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author Esteban Ramírez Martínez
 */
public class Pelicula {

    private int id;
    private String nombre;
    private int generoId;
    private int anioLanzamiento;
    private String imagenSd;
    private String imagenHd;
    private String descripcion;

    public Pelicula() {
    }

    public Pelicula(int id, String nombre, int generoId, int anioLanzamiento, String imagenSd, String imagenHd, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.generoId = generoId;
        this.anioLanzamiento = anioLanzamiento;
        this.imagenSd = imagenSd;
        this.imagenHd = imagenHd;
        this.descripcion = descripcion;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the generoId
     */
    public int getGeneroId() {
        return generoId;
    }

    /**
     * @param generoId the generoId to set
     */
    public void setGeneroId(int generoId) {
        this.generoId = generoId;
    }

    /**
     * @return the anioLanzamiento
     */
    public int getAnioLanzamiento() {
        return anioLanzamiento;
    }

    /**
     * @param anioLanzamiento the anioLanzamiento to set
     */
    public void setAnioLanzamiento(int anioLanzamiento) {
        this.anioLanzamiento = anioLanzamiento;
    }

    /**
     * @return the imagenSd
     */
    public String getImagenSd() {
        return imagenSd;
    }

    /**
     * @param imagenSd the imagenSd to set
     */
    public void setImagenSd(String imagenSd) {
        this.imagenSd = imagenSd;
    }

    /**
     * @return the imagenHd
     */
    public String getImagenHd() {
        return imagenHd;
    }

    /**
     * @param imagenHd the imagenHd to set
     */
    public void setImagenHd(String imagenHd) {
        this.imagenHd = imagenHd;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
